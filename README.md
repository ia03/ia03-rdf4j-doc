# IA03 - TD1 : Apprendre à manipuler RDF4J 

Les énoncés des différents TD sont à retrouver ici :

* [TD1](https://gitlab.com/ia03/ia03-rdf4j-doc/-/blob/master/TD1.md)
* [TD2](https://gitlab.com/ia03/ia03-rdf4j-doc/-/blob/master/TD2.md)
* [TD3](https://gitlab.com/ia03/ia03-rdf4j-doc/-/blob/master/TD3.md)