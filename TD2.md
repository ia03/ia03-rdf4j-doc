# IA03 - TD2 : Créer des requêtes SPARQL avec RDF4J

__Auteur__ : Nicolas Delaforge (nicolas.delaforge@mnemotix.com)

## Etape 1 : Se familiariser avec GraphDB

Connectez-vous à GraphDB à l'adresse suivante [https://dev-graphdb.mnemotix.com/](https://dev-graphdb.mnemotix.com/)

* Login : `ia03-user`
* Mot de passe : `Semantic Web !`

Connectez-vous au repository `ia03-a20`.

Dans l'onglet [SPARQL](https://dev-graphdb.mnemotix.com/sparql), exécutez la requête SPARQL par défaut.


## Etape 2 : Construire un nouveau graphe à partir de données existantes

Le repository `ia03-a20` contient les données qui ont été générées grâce au code du [TD1](https://gitlab.com/ia03/ia03-rdf4j-doc/-/blob/master/TD1.md).

Le modèle de données est le suivant : 

![modele](https://gitlab.com/ia03/ia03-rdf4j-doc/-/raw/master/ia03-model.png)

### Exercice 1 : Générer des URI dans un SELECT

Construisez une requête [`SELECT`](https://www.w3.org/TR/sparql11-query/#select) qui permet de récupérer :

* Toutes les URI de restaurants dans la variable `?r`, leur nom dans la variable `?n` ainsi que le champ `hasCuisine` dans la variable `?c` ordonnées par ordre alphabétique sur la variable `?c` (voir l'opérateur [ORDER BY](https://www.w3.org/TR/sparql11-query/#modOrderBy)).

* À l'aide des opérateurs [`BIND`](https://www.w3.org/TR/sparql11-query/#bind) et [`STRLANG`](https://www.w3.org/TR/sparql11-query/#func-strlang), ajoutez à la requête précédente une variable `?label` qui contient la valeur de la variable `?c` localisée en anglais.

* À l'aide des opérateurs ci-dessous, construisez une variable `?uri` (à remonter dans le `SELECT`) qui concatène le préfixe `http://ns.utc.fr/ia03/concept/` avec le contenu de la variable `?label` en minuscule et encodé.
 	* Par exemple : `http://ns.utc.fr/ia03/concept/fruits%2Fvegetables` pour le label `Fruits/Vegetables@en`
	* Opérateurs à utiliser : [`BIND`](https://www.w3.org/TR/sparql11-query/#bind), [`IRI`](https://www.w3.org/TR/sparql11-query/#func-iri), [`CONCAT`](https://www.w3.org/TR/sparql11-query/#func-concat), [`ENCODE_FOR_URI`](https://www.w3.org/TR/sparql11-query/#func-encode), [`LCASE`](https://www.w3.org/TR/sparql11-query/#func-lcase)

**> Sauvegardez la requête**

### Exercice 2 : Construire les bases d'un vocabulaire contrôlé avec CONSTRUCT

Construisez une requête [`CONSTRUCT`](https://www.w3.org/TR/sparql11-query/#construct) qui permette de :

* Construire un graphe RDF dans lequel les valeurs de la variable `?label` sont les [`skos:prefLabel`](https://www.w3.org/2009/08/skos-reference/skos.html#prefLabel) d'un noeud de type [`skos:Concept`](https://www.w3.org/2009/08/skos-reference/skos.html#Concept) qui a pour URI `?uri`.

* Ajoutez au `CONSTRUCT` un triplet qui déclare l'URI `http://ns.utc.fr/ia03/scheme/cuisineScheme` comme étant de type [`skos:ConceptScheme`](https://www.w3.org/2009/08/skos-reference/skos.html#ConceptScheme) et associez le au `skos:Concept` précédemment généré grâce à la relation [`skos:inScheme`](https://www.w3.org/2009/08/skos-reference/skos.html#inScheme).

**> Sauvegardez la requête**

**> Téléchargez le résultat de la requête au format Turtle**

## Etape 3 : Structurer le vocabulaire contrôlé SKOS dans Protégé

Ouvrez le fichier Turtle généré dans Protégé.

Importez l'ontologie SKOS dans Protégé.

<img src="https://gitlab.com/ia03/ia03-rdf4j-doc/-/raw/master/import-skos-protege.png" style="width:80%;height:80%">

Structurez les concepts entre eux grâce à la relation [`skos:broader`](https://www.w3.org/2009/08/skos-reference/skos.html#broader). Ajoutez les concepts manquants si nécessaires.

<img src="https://gitlab.com/ia03/ia03-rdf4j-doc/-/raw/master/broader-protege.png" style="width:80%;height:80%">

**> Sauvegardez le fichier au format Turtle**

## Etape 4 : Mettre en place un environnement de travail RDF4J

Récupérez le [code source du TD](https://gitlab.com/ia03/ia03-rdf4j/-/archive/step3-build-query/ia03-rdf4j-step3-build-query.zip).

Dézippez dossier.

Ouvrir le projet dans [IntelliJ](https://www.jetbrains.com/fr-fr/idea/download/#) ou [Eclipse](https://www.eclipse.org/downloads/).

Exécutez Maven pour installer les dépendances du projet et compiler les sources.

`mvn clean install package`

Exécutez les tests unitaires pour vous assurer que tout fonctionne correctement.

`mvn test`

Il ne doit pas y avoir d'erreur affichée dans la console.


## Etape 5 : Analyse du code source

Prendre le temps de parcourir le code source et de comprendre les [tests unitaires](https://gitlab.com/ia03/ia03-rdf4j/-/tree/step3-build-query/src/test/java/fr/utc/ia03/rdf4j).

* Le test unitaire `RDF4JClientConfigSpec` permet de valider la classe [RDF4JClientConfig](https://gitlab.com/ia03/ia03-rdf4j/-/blob/step3-build-query/src/main/java/fr/utc/ia03/rdf4j/RDF4JClientConfig.java).

* Le test unitaire `RDF4JClientSpec` permet de valider la classe [RDF4JClient](https://gitlab.com/ia03/ia03-rdf4j/-/blob/step3-build-query/src/main/java/fr/utc/ia03/rdf4j/RDF4JClient.java).

* Le test unitaire `SPARQLQueriesSpec` permet de valider la classe [SPARQLQueries](https://gitlab.com/ia03/ia03-rdf4j/-/blob/step3-build-query/src/main/java/fr/utc/ia03/rdf4j/SPARQLQueries.java).

Exécuter les tests unitaires un par un dans IntelliJ ou Eclipse.

Expliquer à quoi servent les 3 classes qui sont validées par ces tests.

## Etape 6 : Construire des requêtes SPARQL avec RDF4J

En vous inspirant de la classe [SPARQLQueries](https://gitlab.com/ia03/ia03-rdf4j/-/blob/step3-build-query/src/main/java/fr/utc/ia03/rdf4j/SPARQLQueries.java), ajouter une méthode pour sélectionner tous les restaurants de cuisine asiatiques qui se trouvent dans le quartier de "Manhattan".

Voir la documentation du [SparqlBuilder RDF4J](https://rdf4j.org/documentation/tutorials/sparqlbuilder/).

Parser les résultats de la requête et les afficher dans la console sous la forme d'un tableau à 4 colonnes `uri|nom|quartier|cuisine`.