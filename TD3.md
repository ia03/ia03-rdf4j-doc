# IA03 - TD3 : Créer un moteur de recommandation sémantique

__Auteur__ : Nicolas Delaforge (nicolas.delaforge@mnemotix.com)

## Etape 1 : Mise en place de l'environnement de travail

### GraphDB
Télécharger, installer et lancer le serveur [GraphDB Free Edition](https://www.ontotext.com/products/graphdb/graphdb-free/). 

Ouvrir le workbench dans un navigateur à l'adresse : [http://localhost:7200](http://localhost:7200)

Créer un repository nommé `ia03-a2020` (avec les options par défaut).
Dans la section `Setup/Repositories` cliquer sur le lien "Connect repository".

Dans la section `Import/RDF`, charger les fichiers suivants :
* L'ontologie [SKOS](https://gitlab.com/ia03/ia03-rdf4j/-/raw/step4-td3-bootstrap/data/rdf/skos.rdf)
* L'ontologie [FOAF](https://gitlab.com/ia03/ia03-rdf4j/-/raw/step4-td3-bootstrap/data/rdf/foaf.rdf)
* L'ontologie [VCARD](https://gitlab.com/ia03/ia03-rdf4j/-/raw/step4-td3-bootstrap/data/rdf/vcard.ttl)
* L'ontologie [IA03](https://gitlab.com/ia03/ia03-rdf4j/-/raw/step4-td3-bootstrap/data/rdf/ia03.owl)
* L'ontologie [WGS84](https://gitlab.com/ia03/ia03-rdf4j/-/raw/step4-td3-bootstrap/data/rdf/wgs84_pos.rdf)
* La base de connaissance [`restaurants.trig`](https://gitlab.com/ia03/ia03-rdf4j/-/raw/step4-td3-bootstrap/data/rdf/restaurants.trig)
* Le thésaurus [`cuisine-skos.ttl`](https://gitlab.com/ia03/ia03-rdf4j/-/raw/step4-td3-bootstrap/data/rdf/cuisine-skos.ttl)

### IntelliJ
Récupérer le [code source du projet](https://gitlab.com/ia03/ia03-rdf4j/-/archive/step4-td3-bootstrap/ia03-rdf4j-step4-td3-bootstrap.zip).

Dézippez dossier.

Ouvrir le projet dans [IntelliJ](https://www.jetbrains.com/fr-fr/idea/download/#).

Exécutez Maven pour installer les dépendances du projet et compiler les sources.

`mvn clean install package`

## Etape 2 : Analyse du code source

Exécuter la classe [`TD3_RecommendationEngine`](https://gitlab.com/ia03/ia03-rdf4j/-/blob/step4-td3-bootstrap/src/main/java/fr/utc/ia03/TD3_RecommendationEngine.java). 

Prendre le temps d'analyser son code source ainsi que le package [`engine`](https://gitlab.com/ia03/ia03-rdf4j/-/tree/step4-td3-bootstrap/src/main/java/fr/utc/ia03/engine).
 
Identifier les fonctions et les requêtes qui correspondent à chacune des options de la ligne de commande.

## Etape 3 : Exploiter les relations SKOS pour la recherche de restaurants

### Dans GraphDB
Dans le Workbench GraphDB, dans la section `SPARQL`, exécuter les requêtes suivantes :

```
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX : <http://ns.utc.fr/ia03/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT DISTINCT * WHERE {
    ?c a skos:Concept ; skos:prefLabel ?clabel .
    FILTER REGEX(STR(?clabel), "^asian", "i")
}

```
```
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX : <http://ns.utc.fr/ia03/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX wgs: <http://www.w3.org/2003/01/geo/wgs84_pos#>
SELECT DISTINCT ?r ?name ?cuisine ?street ?borough ?postalCode ?locality ?country ?long ?lat 
WHERE {
    ?r a :Restaurant ; 
       foaf:name ?name ; 
       :hasBorough ?borough; 
       :hasCuisine ?cuisine; 
       vcard:hasAddress ?addr .
    ?addr vcard:hasStreetAddress ?street; 
          vcard:hasPostalCode ?postalCode; 
          vcard:hasLocality ?locality; 
          vcard:hasCountryName ?country; 
          wgs:long ?long; 
          wgs:lat ?lat.
    FILTER REGEX(?cuisine, "^asian", "i" )
}
ORDER BY ?cuisine
```

S'inspirer des deux requêtes précédentes pour en créer une nouvelle qui permette de remonter tous les restaurants qui ont des cuisines `narrower`ou `related` au concept `<http://ns.utc.fr/ia03/concept/asian>`.

### Dans IntelliJ
Dans le code JAVA, dans la classe [`SearchByCuisine`](https://gitlab.com/ia03/ia03-rdf4j/-/blob/step4-td3-bootstrap/src/main/java/fr/utc/ia03/engine/SearchByCuisine.java), 
modifiez le code de la fonction [`search`](https://gitlab.com/ia03/ia03-rdf4j/-/blob/step4-td3-bootstrap/src/main/java/fr/utc/ia03/engine/SearchByCuisine.java#L54) et 
remplacez la requête SPARQL d'origine par celle que vous avez créé.

Exécutez le moteur de recommandation pour vous assurer que les résultats sont corrects.

## Etape 4 : Exploiter les coordonnées géographiques

### Dans GraphDB
GraphDB embarque des extensions géographiques qui permettent de faire des traitements supplémentaires sur les coordonnées géospatiales.
[Voir la documentation](https://graphdb.ontotext.com/documentation/standard/geo-spatial-extensions.html)

Dans la base de connaissance [`restaurants.trig`](https://gitlab.com/ia03/ia03-rdf4j/-/raw/step4-td3-bootstrap/data/rdf/restaurants.trig) nous avons utilisé l'ontologie [WGS84](https://www.w3.org/2003/01/geo/) afin de définir les coordonnées géospatiales de nos restaurants (`wgs84:lat` et `wgs84:long`).

Afin d'activer les fonctionnalités géographiques sur le repository `ia03-a2020`, il est nécessaire de demander à GraphDB construire un index géographique.
Pour cela, dans le workbench, dans un onglet `SPARQL`, exécuter la requête suivante :

```
PREFIX ontogeo: <http://www.ontotext.com/owlim/geo#>
INSERT DATA { _:b1 ontogeo:createIndex _:b2. }
```

Une fois cette requête exécutée, les fonctionnalités de requétage geospatiales sont disponibles.

Sachant que les coordonnées géographiques de Central Park sont :

* __Latitude__ : `40.771133`
* __Longitude__ : `-73.974187`

Voici la requête qui donne la liste de tous les restaurants qui se trouvent à moins d'un kilomètre de Central Park.

```
PREFIX wgs: <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX omgeo: <http://www.ontotext.com/owlim/geo#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX : <http://ns.utc.fr/ia03/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT distinct ?distStr ?r ?name ?cuisine ?street ?postalCode ?locality WHERE {

    BIND("40.771133"^^xsd:decimal as ?centralParkLat)
    BIND("-73.974187"^^xsd:decimal as ?centralParkLong)

    ?addr omgeo:nearby(?centralParkLat ?centralParkLong "1km"); 
		vcard:hasStreetAddress ?street; 
  		vcard:hasPostalCode ?postalCode; 
    	vcard:hasLocality ?locality ; 
     	wgs:lat ?lat ; 
      	wgs:long ?long .

    ?r vcard:hasAddress ?addr; 
        :hasCuisine ?cuisine; 
        foaf:name ?name .

    BIND(omgeo:distance(?lat, ?long, ?centralParkLat, ?centralParkLong) as ?distance)
    BIND(concat(str(round(?distance * 1000)), "m") as ?distStr)
}
ORDER BY ASC(?distance)
```

En vous inspirant de cette requête, écrivez une requête, pour une URI de restaurant donnée, qui permette de retrouver tous les restaurants ayant la même cuisine et situés à moins de 2km.

### Dans IntelliJ

Dans le code JAVA, dans la classe [`SearchByDistance`](https://gitlab.com/ia03/ia03-rdf4j/-/blob/step4-td3-bootstrap/src/main/java/fr/utc/ia03/engine/SearchByDistance.java), 
modifiez le code de la fonction [`search`](https://gitlab.com/ia03/ia03-rdf4j/-/blob/step4-td3-bootstrap/src/main/java/fr/utc/ia03/engine/SearchByDistance.java#L40) et 
remplacez la requête SPARQL d'origine par celle que vous avez créé.

Exécutez le moteur de recommandation pour vous assurer que les résultats sont corrects.

## Etape 5 : Pour aller plus loin

### Dans GraphDB
Faire une requête qui permette de retrouver tous les restaurants situés à moins de 3km du restaurant "L. A. Pizzeria" 
dont l'URI est `<http://ns.utc.fr/ia03/restaurant/40594166>`et qui ont une cuisine `narrower` ou `related` de celui-ci.

Trier les résultats par distance croissante.